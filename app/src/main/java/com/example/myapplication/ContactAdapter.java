package com.example.myapplication;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder> {

    private List<Contact> listaKontakta = new ArrayList<>();

    public ContactAdapter(List<Contact> listaKontakta) {
        this.listaKontakta = listaKontakta;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitems, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

       final Contact trenutniItem = listaKontakta.get(position);
        holder.setItem(trenutniItem);

        //uklanjanje kontakta na klik
        holder.parent_layout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                int position= listaKontakta.indexOf(trenutniItem);
                listaKontakta.remove(position);
                notifyItemRemoved(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return listaKontakta.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView nameTextView, numbersTextView;
        private LinearLayout parent_layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.name);
            numbersTextView = itemView.findViewById(R.id.numbersList);
            parent_layout = itemView.findViewById(R.id.parent_layout);
        }

        public void setItem(Contact trenutniItem) {

            nameTextView.setText(trenutniItem.name);
            numbersTextView.setText(trenutniItem.numbers.toString());
          if (trenutniItem.name.startsWith("L")) {
              parent_layout.setBackgroundColor(Color.YELLOW);
          }
          else{
              parent_layout.setBackgroundColor(Color.WHITE);
          }
        }
    }

}
