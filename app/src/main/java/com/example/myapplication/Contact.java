package com.example.myapplication;

import java.util.ArrayList;
import java.util.List;

public class Contact {
    String name;
    List<String> numbers;

    public Contact(String name, List<String> numbers) {
        this.name=name;
        this.numbers=numbers;
    }

}
