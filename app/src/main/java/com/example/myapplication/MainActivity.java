package com.example.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import static androidx.core.util.PatternsCompat.EMAIL_ADDRESS;

public class MainActivity extends AppCompatActivity {

    private SharedPreferences prefs;
    private Integer otvaranjeUOvomPokretanjuCounter = 0;

    public final static boolean isValidEmail(String email) {
        if (TextUtils.isEmpty(email)) {
            return false;
        } else {
            return EMAIL_ADDRESS.matcher(email).matches();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = (Button) findViewById(R.id.activityMain_button);
        TextView title = (TextView) findViewById(R.id.mainActivity_title);

        button.setOnClickListener(new MyOnClickListener(title));

        //broj pokretanja aplikacije ikada
        prefs = getPreferences(Context.MODE_PRIVATE);
        Integer pokretanjeCounter = prefs.getInt("pokretanjeCounter", 0);
        pokretanjeCounter++;
        prefs.edit()
                .putInt("pokretanjeCounter", pokretanjeCounter)
                .apply();

        TextView pokretanjeCounterView = (TextView) findViewById(R.id.mainActivity_pokretanjeCounter);
        pokretanjeCounterView.setText("Pokretanje aplikacije ikada: " + pokretanjeCounter.toString());
    }

    @Override
    protected void onResume() {
        super.onResume();

        //broj otvaranje aplikacije ikada
        Integer otvaranjeCounter = prefs.getInt("otvaranjeCounter", 0);
        otvaranjeCounter++;
        prefs.edit()
                .putInt("otvaranjeCounter", otvaranjeCounter)
                .apply();
        TextView otvaranjeCounterView = (TextView) findViewById(R.id.mainActivity_otvaranjeCounter);
        otvaranjeCounterView.setText("Otvaranje aplikacije ikada: " + otvaranjeCounter.toString());

        //broj otvaranja aplikacije u ovomo pokretanju
        otvaranjeUOvomPokretanjuCounter++;

        TextView otvaranjeUOvomPokretanjuCounterView = (TextView) findViewById(R.id.mainActivity_otvaranjeUOvomPokretanjeCounter);
        otvaranjeUOvomPokretanjuCounterView.setText("Otvaranje u ovom pokretanju: " + otvaranjeUOvomPokretanjuCounter.toString());

    }

    public class MyOnClickListener implements View.OnClickListener {

        private final TextView title;
        private Integer counterButton = 0;

        public MyOnClickListener(TextView title) {

            this.title = title;
        }

        @Override
        public void onClick(View v) {
            counterButton++;

            Integer kliknuoButtonCounter = prefs.getInt("kliknuoButtonCounter", 0);
            kliknuoButtonCounter++;
            prefs.edit()
                    .putInt("kliknuoButtonCounter", kliknuoButtonCounter)
                    .apply();

            title.setText("Kliknuo si me " + counterButton + " puta u ovom pokretanju te " + kliknuoButtonCounter + " ikada");

            //provjera passworda
            TextView password = (TextView) findViewById(R.id.mainActivity_passwordEditText);
            String passwordString = password.getText().toString();
            boolean isPasswordValid=false;

            if (!passwordString.equals("")) {
                long count = passwordString.toCharArray().length;
                if (count < 6) {
                    Toast.makeText(getApplicationContext(), "Lozinka je manja od 6 znakova!", Toast.LENGTH_SHORT).show();
                } else {
                    isPasswordValid=true;
                }
            }

            //provjera e-mail
            EditText emailValidate = (EditText) findViewById(R.id.mainActivity_emailEditText);

            String email = emailValidate.getText().toString().trim();
            boolean validEmail = isValidEmail(email);

            if (!validEmail) {
                Toast.makeText(getApplicationContext(), "Netocna e-mail adresa", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "Uspjesan unos!", Toast.LENGTH_SHORT).show();
            }

            //otvaranje novog prozora
            if(isPasswordValid && validEmail) {
                Intent intent = new Intent(MainActivity.this, OnClickButtonActivity.class);
                startActivity(intent);
            }
        }

    }
}