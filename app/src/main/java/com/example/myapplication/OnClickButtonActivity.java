package com.example.myapplication;

import android.content.ContentResolver;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OnClickButtonActivity extends AppCompatActivity {

    private List<Contact> listaKontakta = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_click_button);

        loadContacts();

        initRecyclerView();

    }

    private void loadContacts() {

        ContentResolver contentResolver = getContentResolver();
        Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                String id = cursor.getString((cursor.getColumnIndex(ContactsContract.Contacts._ID)));
                String name = cursor.getString((cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));
                int hasPhoneNumber = Integer.parseInt(cursor.getString((cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))));
                List<String> numbers =new ArrayList<>();

                if (hasPhoneNumber > 0) {
                    Cursor cursor2 = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " =?",
                            new String[]{id},
                            null);
                    while (cursor2.moveToNext()) {
                        String phoneNumber = cursor2.getString((cursor2.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
                        numbers.add(phoneNumber);
                    }
                    cursor2.close();
                }

                listaKontakta.add(new Contact(name,  numbers));
            }
        }
        cursor.close();
        Collections.sort(listaKontakta, new ContactsComparator());
    }

    private void initRecyclerView() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.onClickButtonActivity_recyclerView);
        ContactAdapter contactAdapter = new ContactAdapter(listaKontakta);
        recyclerView.setAdapter(contactAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }
}
